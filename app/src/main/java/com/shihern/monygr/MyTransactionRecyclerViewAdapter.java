package com.shihern.monygr;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shihern.monygr.TransactionContent.Transaction;
import com.shihern.monygr.TransactionsFragment.OnListFragmentInteractionListener;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Transaction} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyTransactionRecyclerViewAdapter extends RecyclerView.Adapter<MyTransactionRecyclerViewAdapter.ViewHolder> {

    private final List<Transaction> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyTransactionRecyclerViewAdapter(List<Transaction> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_transaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitle.setText(mValues.get(position).title);
        holder.mDescription.setText(mValues.get(position).description);
        holder.mCategory.setText(mValues.get(position).category);

        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String amount = fmt.format(mValues.get(position).amount);
        holder.mAmount.setText(amount);

        Date date = new Date(mValues.get(position).date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault());
        holder.mDate.setText(sdf.format(date));

        //Code to set transitionname for the animation between the list and transaction details
        //set the views in this list item to have transition names. These transition names are unique
        //and different from that of other list items due to adding position to it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.mTitle.setTransitionName("trans_title" + position);
            holder.mDescription.setTransitionName("trans_description" + position);
            holder.mCategory.setTransitionName("trans_category" + position);
            holder.mAmount.setTransitionName("trans_amount" + position);
            holder.mDate.setTransitionName("trans_date" + position);
        }

        //When the list item is clicked, call the onListFragmentInteraction of the listener (in this case
        //the fragment containing the list)
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    TextView title = (TextView)v.findViewById(R.id.transaction_title);
                    TextView description = (TextView)v.findViewById(R.id.transaction_description);
                    TextView category = (TextView)v.findViewById(R.id.transaction_category);
                    TextView amount = (TextView)v.findViewById(R.id.transaction_amount);
                    TextView date = (TextView)v.findViewById(R.id.transaction_date);

                    //put in values, and references to the views in this list item
                    mListener.onListFragmentInteraction(holder.mItem, title, description, category, amount, date);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle;
        public final TextView mDescription;
        public final TextView mCategory;
        public final TextView mAmount;
        public final TextView mDate;
        public Transaction mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView)view.findViewById(R.id.transaction_title);
            mDescription = (TextView)view.findViewById(R.id.transaction_description);
            mCategory = (TextView)view.findViewById(R.id.transaction_category);
            mAmount = (TextView)view.findViewById(R.id.transaction_amount);
            mDate = (TextView)view.findViewById(R.id.transaction_date);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

}
